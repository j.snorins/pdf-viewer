## Deployment instructions:

You **must have** docker installed on your system

`https://docs.docker.com/get-docker/`

You **must have** docker-compose installed on your system

`https://docs.docker.com/compose/install/`

Application operates on ports `777` (frontend) and `555` (backend), if these ports are not available on your system please choose another ones by modifying `docker-compose.yaml` file and `frontend/.env` file REACT_APP_BACKEND_PORT variable value

As aplication is using internal communication with the database and backend application from webserver, we need to create separate docker network not to have any collisions by executing:

`docker network create my-network`

After this we can start the application framework by executing command:

`docker-compose up -d`

After this we must install composer dependencies of the backend server:

`docker-compose exec app composer install`

Once this is done we need to run migrations to create database tables:

`docker-compose exec app php artisan migrate`

To run tests please use command:

`docker-compose exec app vendor/bin/phpunit`

Navigate your browser to application: e.g. `http://localhost:777` 

start using application!