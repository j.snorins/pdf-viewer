FROM php:7.3-fpm

RUN apt-get update && apt-get install -y libzip-dev zip libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-install pcntl \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install zip

RUN apt-get install -y ghostscript

COPY ./config/policy.xml /etc/ImageMagick-6/policy.xml

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
WORKDIR /var/www