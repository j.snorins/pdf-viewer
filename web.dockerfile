FROM node:13.12.0 AS development

WORKDIR /code
COPY ./frontend/package.json ./frontend/package-lock.json /code/
RUN npm ci
COPY ./frontend/src /code/src
COPY ./frontend/public/index.html /code/public/index.html
COPY ./frontend/.env /code/.env

CMD ["npm", "start"]

FROM development AS builder

RUN npm run build

FROM nginx:1.17.9 AS production
COPY --from=builder /code/build /var/www
ADD config/frontend.vhost.conf /etc/nginx/conf.d/default.conf
