import {Document, Page, pdfjs} from 'react-pdf';
import React, {Component} from "react";

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

class PDFDocument extends Component {
    constructor() {
        super();
        this.state = {
            numPages: 0,
            url: null
        };

        this.onDocumentLoadSuccess = this.onDocumentLoadSuccess.bind(this);
    }

    componentDidMount() {
        const url = `http://${process.env.REACT_APP_BACKEND_URL}:${process.env.REACT_APP_BACKEND_PORT}/pdf/view_file/${this.props.openPdf}`;
        this.setState({url: url});

    }

    componentWillUnmount() {
        this.setState({url: null});
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextState.url !== this.state.url) {
            return true;
        }
        if (nextState.numPages !== this.state.numPages) {
            return true;
        }
        return false;
    }

    onDocumentLoadSuccess = (value) => {
        this.setState({numPages: value.numPages});
    }

    render = () => {
        let pages = [];
        for (let i = 1; i <= this.state.numPages; i++) {
            pages.push(<Page pageNumber={i}/>);
        }
        return (
            <div>
                {this.state.url && <Document file={{
                    url: this.state.url
                }} onLoadSuccess={this.onDocumentLoadSuccess}>
                    {pages}
                </Document>}
            </div>
        )
    }
}

export default PDFDocument;
