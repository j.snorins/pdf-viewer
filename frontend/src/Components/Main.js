import React, {Component} from "react";
import {
    Route,
    NavLink,
    HashRouter
} from "react-router-dom";
import Home from "../Pages/Home";
import Add from "../Pages/Add";


class Main extends Component {
    render() {
        return (
            <HashRouter>
            <div>
                <h1>Pdf viewer</h1>
                <ul className="header">
                    <li>
                        <NavLink exact to="/">List</NavLink>
                    </li>
                    <li>
                        <NavLink to="/new">Add new document</NavLink>
                    </li>
                </ul>
                <div className="content">
                    <Route exact path="/" component={Home}/>
                    <Route path="/new" component={Add}/>
                </div>
            </div>
            </HashRouter>
        );
    }
}

export default Main;