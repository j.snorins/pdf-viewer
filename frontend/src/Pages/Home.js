import React, {Component} from "react";
import axios from 'axios';
import Modal from 'react-modal';
import PDFDocument from '../Components/PDFDocument'
import ReactPaginate from 'react-paginate';

Modal.setAppElement('#root')

class Home extends Component {
    constructor() {
        super();
        this.state = {
            showModal: false,
            pdfs: [],
            openPdf: null,
            page: 1,
            pageCount: 0
        };
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleChangeActivePDF = this.handleChangeActivePDF.bind(this);
        this.changePage = this.changePage.bind(this);
    }

    componentDidMount() {
        this.loadPage(1);
    }

    handleCloseModal() {
        this.setState({showModal: false});
    }

    handleChangeActivePDF(value) {
        this.setState({openPdf: value, showModal: true});
    }

    changePage(value) {
        const self = this;
        let selectedPage = value.selected + 1;
        this.setState({page: selectedPage}, self.loadPage(selectedPage));
    }

    loadPage(value) {
        axios.get(`http://${process.env.REACT_APP_BACKEND_URL}:${process.env.REACT_APP_BACKEND_PORT}/pdf/show/${value}`).then(response => {
            this.setState({
                pdfs: response.data.pdfs,
                pageCount: Math.ceil(response.data.total / 20)
            });
        });
    }

    render() {
        const COLS = 4;
        const elements = this.state.pdfs;
        const items = []
        for (const [index, value] of elements.entries()) {
            items.push(<div key={index} onClick={() => this.handleChangeActivePDF(value.id)} class="img-thumbnail"><img
                src={`http://${process.env.REACT_APP_BACKEND_URL}:${process.env.REACT_APP_BACKEND_PORT}/pdf/view_thumbnail/${value.id}`} alt=''/></div>);
            if (index !== 0 && index % COLS === COLS - 1) {
                items.push(<div key={`break${index}`} class="break"/>)
            }
        }
        return (
            <div>
                {this.state.pageCount > 1 &&
                <ReactPaginate pageCount={this.state.pageCount} initialPage={0} pageRangeDisplayed={1}
                               marginPagesDisplayed={1} onPageChange={this.changePage} containerClassName="paginator"/>}
                <div>
                    {items}
                </div>
                <div style={{width: "600px", height: "200px"}}>
                    <Modal
                        isOpen={this.state.showModal}>
                        <button onClick={this.handleCloseModal}>X</button>
                        <PDFDocument modalOpen={this.state.showModal} openPdf={this.state.openPdf}/>
                    </Modal>
                </div>
            </div>
        );
    }
}

export default Home;