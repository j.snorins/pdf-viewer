import React, {Component} from "react";
import {Redirect} from 'react-router';
import axios from "axios";

class Add extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFile: null,
            readyToUpload: false,
            uploaded: false,
            uploading: false
        }
    }

    onChangeHandler = event => {
        this.setState({
            selectedFile: event.target.files[0],
            loaded: 0,
            readyToUpload: true
        });
    }
    onClickHandler = () => {
        const data = new FormData();
        data.append('file', this.state.selectedFile);

        console.log(process.env);
        console.log(`http://${process.env.REACT_APP_BACKEND_URL}:${process.env.REACT_APP_BACKEND_PORT}/pdf/upload`);

        this.setState({uploading: true});

        axios.post(`http://${process.env.REACT_APP_BACKEND_URL}:${process.env.REACT_APP_BACKEND_PORT}/pdf/upload`, data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            if (res.data && res.data.success === true) {
                this.setState({
                    uploaded: true
                });
            }
        });
    }

    render() {
        if (this.state.uploaded) {
            return <Redirect push to="/"/>;
        }
        return (
            <div>
                {this.state.uploading === false &&
                <form>
                    <input type="file" name="pdf" accept="application/pdf" onChange={this.onChangeHandler}/>
                    {this.state.readyToUpload && <button type="button" className="btn btn-success btn-block"
                                                         onClick={this.onClickHandler}>Upload</button>}
                </form>}
                {this.state.uploading && <div class="uploading">Uploading</div>}
            </div>
        );
    }
}

export default Add;