<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Models\Pdf;
use Laravel\Lumen\Routing\Router;

$router->get(
    '/',
    function () use ($router) {
        $pdfs = new Pdf;
        $pdfs = $pdfs->listAll();

        $newPdf = new Pdf;
        $newPdf->save();

        return view('index', ['pdfs' => $pdfs]);
    }
);

$router->group(
    [
        'prefix' => 'pdf',
    ],
    function () use ($router) {
        $router->get('show/{page:\d+}', 'PdfController@show');
        $router->get('view_file/{id:\d+}', 'PdfController@get');
        $router->get('view_thumbnail/{id:\d+}', 'PdfController@preview');
        $router->post('upload', 'PdfController@upload');
    }
);
