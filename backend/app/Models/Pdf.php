<?php

namespace App\Models;

/**
 * Class Pdf
 * @package App\Models
 *
 * @property int id
 * @property string path
 * @property string thumb_path
 */
final class Pdf extends BaseModel
{
    public function listAll()
    {
        return Pdf::all();
    }
}
