<?php

namespace App\Http\Services;


use App\Models\Pdf;
use Illuminate\Filesystem\Filesystem;

final class PdfService
{
    const PAGE_LIMIT = 20;
    private $pdf;

    private $extension = 'png';

    public function __construct()
    {
        $this->pdf = new Pdf;
    }

    public function getCount()
    {
        return $this->pdf->all()->count();
    }

    public function listPdfs(int $page)
    {
        return $this->pdf->orderBy('created_at')->get()->skip(self::PAGE_LIMIT * ($page - 1))->take(20)->values();
    }

    public function uploadFile($file)
    {
        $path = storage_path('app/pdfs/');

        $file->move($path, $file->getClientOriginalName());

        $this->pdf->path = $path . $file->getClientOriginalName();

        $this->pdf->thumb_path = $this->generateThumbnail($path . $file->getClientOriginalName());

        if ($this->pdf->save()) {
            return true;
        }
        return false;
    }

    public function generateThumbnail(string $path)
    {
        $pdf = new \Spatie\PdfToImage\Pdf($path);

        $pdf->setOutputFormat($this->extension);
        $pdf->setResolution(12);
        
        $fileParts = explode('.', $path);
        $outputName = reset($fileParts) . '.' . $this->extension;

        if ($pdf->saveImage($outputName) !== false) {
            return $outputName;
        }

        return false;
    }

    public function outputImage(int $id)
    {
        $pdf = Pdf::find($id);
        $fileSystem = new Filesystem;
        $file = $fileSystem->get($pdf->path);

        $fileParts = explode('/', $pdf->path);
        $fileName = end($fileParts);

        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline;'

        ];

        return response()->download($pdf->path);
    }

    public function outputThumbnail(int $id)
    {
        $pdf = Pdf::find($id);
        $fileSystem = new Filesystem;
        $file = $fileSystem->get($pdf->thumb_path);

        $headers = [
            'Content-Type' => 'image/png',
            'Content-Disposition' => 'inline;'
        ];

        return response()->stream(
            function () use ($file) {
                echo $file;
            },
            200,
            $headers
        );
    }
}