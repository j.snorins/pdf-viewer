<?php

namespace App\Http\Controllers;

use App\Http\Services\PdfService;
use App\Models\Pdf;
use Illuminate\Http\Request;

class PdfController extends BaseController
{
    public function show(int $page, PdfService $pdfService)
    {
        $pdfs = $pdfService->listPdfs($page);
        $total = $pdfService->getCount();
        return response()->json([
            'pdfs' => $pdfs,
            'total' => $total
        ]);
    }

    public function upload(Request $request, PdfService $pdfService)
    {
        if (!$request->hasFile('file')) {
            return response()->json(['upload_file_not_found'], 400);
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            return response()->json(['invalid_file_upload'], 400);
        }

        $uploaded = $pdfService->uploadFile($file);
        return response()->json(['success' => $uploaded]);
    }

    public function get(int $id, PdfService $pdfService)
    {
        return $pdfService->outputImage($id);
    }

    public function preview(int $id, PdfService $pdfService)
    {
        return $pdfService->outputThumbnail($id);
    }
}
