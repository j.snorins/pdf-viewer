<html>
    <body>
        <h1>Pdf viewer</h1>
    <table border="1">
        <?php
        const COLS = 5;
        const ROWS = 4;
        use App\Models\Pdf;

        $rowCounter = 0;
        $activeRecord = 0;

        /** @var Pdf $pdfs */
        while (count($pdfs) > $activeRecord) {
            if ($activeRecord % COLS === 0) {
                if ($activeRecord > 0) {
                    echo '</tr>' . PHP_EOL;
                }

                if ($activeRecord > COLS * ROWS) {
                    break;
                }
                echo '<tr>';
            }

            if ($activeRecord > 0) {
                echo '</td>' . PHP_EOL;
            }
            echo '<td>';

            echo '*' . $activeRecord++ . '*';
        }

        ?>
    </table>
    </body>
</html>