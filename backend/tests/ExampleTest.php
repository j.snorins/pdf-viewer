<?php

use App\Models\Pdf;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasic()
    {
        $this->get('/pdf/show/1');

        $this->assertEquals(
            collect(['pdfs' => [], 'total' => 0])->toJson(), $this->response->getContent()
        );
    }

    public function testFileUpload()
    {
        $stub = __DIR__ . '/assets/sample.pdf';
        $name = Carbon\Carbon::now()->toDateTimeString() . '.png';
        $path = sys_get_temp_dir() . '/' . $name;

        copy($stub, $path);

        $file = [];
        $this->call('POST', '/pdf/upload', [], [], ['file' => $file], ['Accept' => 'application/pdf']);
        $this->assertResponseStatus(Response::HTTP_BAD_REQUEST);

        $file = new UploadedFile($path, $name, 'application/pdf', null, true);
        $response = $this->call('POST', '/pdf/upload', [], [], ['file' => $file], ['Accept' => 'application/pdf']);

        $this->assertResponseOk();
        $content = json_decode($response->getContent());
        $this->assertObjectHasAttribute('success', $content);

        $uploaded = storage_path('app/pdfs/' . $name);
        $this->assertFileExists($uploaded);

        $this->get('/pdf/show/1')->seeJsonStructure(
            [
                'pdfs' => [
                    ['id']
                ],
                'total'
            ]
        );
        @unlink($uploaded);
    }
}
